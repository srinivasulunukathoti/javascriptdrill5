// Check the each person projects list and group the data based on the project status and return the data as below
  
function groupTheProjectsBasedOnStatus(data) {
    const groupedData = {};
    // Iterate each person project list and grouping the data
    data.forEach(person => {
        person.projects.forEach(project => {
            const { name, status } = project;
            if (!groupedData[status]) {
                groupedData[status] = [];
            }
            // adding the data
            groupedData[status].push(name);
        });
    });
    // returning the data
    return groupedData;

}
// exports the code
module.exports = {groupTheProjectsBasedOnStatus};
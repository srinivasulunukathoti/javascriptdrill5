// From the given data, filter all the unique langauges and return in an array
  
function findAllUniqueLangauges(data) {
    const uniqueLanguagesSet = new Set();

    // Iterate over each person's languages and add them to the set
    data.forEach(person => {
        person.skills.forEach(skills => {
            uniqueLanguagesSet.add(skills);
        });
    });

    // Convert Set to an array
    const uniqueLanguagesArray = Array.from(uniqueLanguagesSet);
    return uniqueLanguagesArray;
}
// exports the code
module.exports = {findAllUniqueLangauges};
const groupTheProjectsBasedOnStatus = require('../problem2');
const data = require('../data');

try {
    const projectsDetails = groupTheProjectsBasedOnStatus.groupTheProjectsBasedOnStatus(data.data);
    console.log(projectsDetails);
} catch (error) {
    console.log("data not found");
}
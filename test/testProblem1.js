const findDetailsBasedOnEmail = require('../problem1');
const data = require('../data');

try {
    const detailsOfPersons = findDetailsBasedOnEmail.findDetailsBasedOnEmail(data.data);
    console.log(detailsOfPersons);
} catch (error) {
    console.log("Data not found");
}
// Iterate over each object in the array and in the email breakdown the email and return the output as below:

function findDetailsBasedOnEmail(data) {
   // intialize an array to store persional details.
    let personsDetails = data.map(person => {
        // extracting firstName , lastName and emailDomain
        let [firstName , lastName] = person.email.split('@')[0].split('.');
        // Capatilize  first character as first name and lastname. 
        return {
                 firstName : firstName.charAt(0).toUpperCase()+firstName.slice(1),
                 lastName : lastName.charAt(0).toUpperCase()+lastName.slice(1),
                 emailDomain : person.email.split('@')[1]
        };
    });
    return personsDetails;
}
// exports the code
module.exports = {findDetailsBasedOnEmail};